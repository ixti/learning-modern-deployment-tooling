default:
	echo "wut?"

podman-build:
	podman build . --file Containerfile --tag demo-app:latest

podman-compose-up: podman-build
	podman pod create --replace --publish 3000:3000 --name demo-app
	podman run --rm --detach --pod demo-app demo-app:latest
	podman run --rm --detach --pod demo-app redis:alpine

podman-compose-down:
	podman pod rm demo-app --force

docker-build:
	docker build . --file Containerfile --tag demo-app:latest

docker-compose-up: docker-build
	docker-compose --file compose.yaml up --detach --force-recreate

docker-compose-down:
	docker-compose --file compose.yaml down
