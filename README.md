# Podman

- Install podman
- Start pod: `make podman-compose-up`
- Stop pod: `make podman-compose-down`

# Docker

- Install docker and docker-compose
- Start pod: `make docker-compose-up`
- Stop pod: `make docker-compose-down`

# Minikube

- Install minikube
  - Fedora: `sudo dnf install https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm`

## Start Minikube

    minikube start

On my machine it started cluser with VirtualBox.
Instead of VB we can start it with docker (preferred) or podman (experimental):

    minikube start --driver=docker
    minikube start --driver=podman

This will require `containerd 1.4+`. Also, podman will require `sudo podman` to
be allowed for password-less execution.

## Build image

    docker build . --file Containerfile --tag demo-app:latest
    minikube image load --daemon demo-app:latest
    minikube kubectl -- create deployment demo-app --image=demo-app:latest
    minikube kubectl -- create deployment redis --image=redis:latest


---

https://minikube.sigs.k8s.io/docs/
https://minikube.sigs.k8s.io/docs/handbook/kubectl/

https://stackoverflow.com/questions/42564058/how-to-use-local-docker-images-with-minikube
