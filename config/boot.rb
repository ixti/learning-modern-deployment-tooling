# frozen_string_literal: true

require "bundler/setup"
Bundler.require(:default)

Redis.current = Redis.new(url: ENV.fetch("REDIS_URL", "redis://localhost:6379"))
