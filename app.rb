# frozen_string_literal: true

require_relative "./config/boot"

class Application < Sinatra::Base
  get "/" do
    name = Redis.current.get("username").to_s.strip
    name = "Anonymous" if name.empty?

    slim :index, :locals => { :name => name }
  end

  post "/rename" do
    Redis.current.set("username", params[:name])
    redirect "/", 303
  end
end
